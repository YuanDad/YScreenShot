# YScreenShot

# 自我介绍
史上最完整的QQ截图工具，1比1，高仿供学习，MulanPSL-2.0协议，Qt实现，可随意移植国产系统UOS/麒麟。

仿最新QQ截图，中国码云独家开源, 可以随意修改使用优化源码。

[CSDN](https://blog.csdn.net/qq_36145663/article/details/121951931)

# 软件功能
1. 支持窗口自动查找
2. 支持桌面大小信息、鼠标坐标信息、RGB值信息动态显示
3. 支持像素放大镜
4. 支持RGB颜色拾取
5. 支持画刷大小调节，颜色调节
6. 支持箭头调节
7. 支持方形调节
8. 支持椭圆调节
9. 支持移动
10. 支持钉在桌面上
11. 支持撤销
12. 支持工具栏位置自适应调节
13. 支持保存图片
14. 支持保存剪贴板
15. 支持录制GIF
16. 支持录制AVI

# 瓶颈功能(因为考虑到跨平台，所以功能实现不能限制在单一平台上)

1. 长截图
2. 翻译
3. 文字识别
4. 录制mp4

# 作者自述

做这个开源，我承认我有赌的成分，但我也有个赚十个亿的梦...