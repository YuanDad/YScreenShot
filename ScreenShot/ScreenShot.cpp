#include "ScreenShot.h"
#include "YCenterScrollArea.h"
#include "ScreenShotWidget.h"
#include "Yuan.h"
#include <QDebug>
ScreenShot::ScreenShot(QWidget *parent) : QWidget(parent)
{
    YCenterScrollArea* area = new YCenterScrollArea(this);
    area->defaultLayout();
    area->setCenterWidget(Yuan::triggerWidget([this]{
        ScreenShotWidget* widget = new ScreenShotWidget;
        connect(widget, &ScreenShotWidget::deleteLated, this, &ScreenShot::show);
        connect(widget, &ScreenShotWidget::destroyed, this, &ScreenShot::show);
    }, this));
    resize(400, 300);
}

ScreenShot::~ScreenShot()
{
}
