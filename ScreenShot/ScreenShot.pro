QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ScreenShot

SOURCES += \
    DrawTools.cpp \
    MediaTools.cpp \
    YImageDrawer.cpp \
    YToolWidget.cpp

DEFINES += QT_DEPRECATED_WARNINGS

CONFIG += c++11

SOURCES += \
        ScreenShot.cpp \
        ScreenShotWidget.cpp \
        YCenterScrollArea.cpp \
        YMagnifier.cpp \
        StickyNote.cpp \
        Tools.cpp \
        Yuan.cpp

HEADERS += \
        DrawTools.h \
        MediaTools.h \
        ScreenShot.h \
        ScreenShotWidget.h \
        StickyNote.h \
        Tools.h \
        YCenterScrollArea.h \
        YImageDrawer.h \
        YMagnifier.h \
        YToolWidget.h \
        Yuan.h

FORMS += \
    MediaTools.ui \
    Tools.ui \
    DrawTools.ui \
    test.ui

RESOURCES += \
    Resource/resource.qrc

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

include($$PWD/QyMultiMedia/QyMultiMedia.pri)

TEMPLATE = app
SOURCES += main.cpp
