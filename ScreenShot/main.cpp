#include "ScreenShot.h"
#include <QApplication>
#include <QFontDatabase>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFontDatabase::addApplicationFont((":/FontAwesome.ttf"));

    ScreenShot w;
    w.show();

    return a.exec();
}
