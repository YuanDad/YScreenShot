#ifndef YCENTERSCROLLAREA_H
#define YCENTERSCROLLAREA_H

#include <QScrollArea>

class YCenterScrollArea : public QScrollArea
{
    Q_OBJECT
public:
    explicit YCenterScrollArea(QWidget *parent = nullptr);
    void defaultLayout();

    YCenterScrollArea* setCenterWidget(QWidget* centerWidget);
};

#endif // YCENTERSCROLLAREA_H
