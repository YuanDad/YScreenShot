﻿#ifndef TOOLS_H
#define TOOLS_H

#include "YToolWidget.h"

namespace Ui {
class Tools;
}

class Tools : public YToolWidget
{
    Q_OBJECT

public:
    explicit Tools(QWidget *parent = nullptr);
    ~Tools();
    void setTack();

signals:
    void draw(int type);//绘画
    void redo();
    void tack();//钉住
    void record();//录制
    void download();//下载
    void clipBoard();//下载
    void finished();//完成退出

private:
    Ui::Tools *ui;
};

#endif // TOOLS_H
