#ifndef STICKYNOTE_H
#define STICKYNOTE_H

#include <QWidget>

class Tools;
class StickyNote : public QWidget
{
    Q_OBJECT
public:
    explicit StickyNote(const QImage& image, QWidget *parent = nullptr);
    ~StickyNote() override;

signals:

public slots:

public:
    QImage getScreenShotImage();
    void initToolConnect();
    void clearTimer();

private:
     QImage image;
     Tools* tools;
     QPoint pressedPoint;
     int timerId = -1;

protected:
     void paintEvent(QPaintEvent*) override;
     void mousePressEvent(QMouseEvent*) override;
     void mouseMoveEvent(QMouseEvent*) override;
     void mouseReleaseEvent(QMouseEvent*) override;
     void focusInEvent(QFocusEvent*) override;
     void focusOutEvent(QFocusEvent*) override;
     void timerEvent(QTimerEvent*) override;
};

#endif // STICKYNOTE_H
