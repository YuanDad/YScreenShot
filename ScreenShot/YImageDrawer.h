﻿#ifndef YIMAGEDRAWER_H
#define YIMAGEDRAWER_H

#include <QWidget>

class QGraphicsView;
class QGraphicsScene;
class QGraphicsItem;
class DrawTools;
namespace Y_Image_Drawer_GUI {
class YGraphicsItem;
class YGraphicsMosaicItem;
}
class YImageDrawer : public QWidget
{
    Q_OBJECT

public:
    enum Type{
        Rect = 1, Ellipse, Arrow, Pen, Mosaic, Text, Tip
    };
public:
    explicit YImageDrawer(const QImage& image, QWidget *parent = nullptr);
    void setType(int t);
    void setViewCursor(const QCursor& cursor);
    void setTools(DrawTools* t);

public:
    DrawTools* tools;
signals:

public slots:
    void thicknessChanged(int);
    void colorChanged(QColor);
    void fontChanged(const QFont&);
    void vagueChanged(int);
    void redo();

protected:
    void createItem(const QPoint& pos);

private:
    QImage image;
    QGraphicsView* view;
    QGraphicsScene* scene;
    Type type;
    Y_Image_Drawer_GUI::YGraphicsItem* currentItem = nullptr;
    bool pressed = false;
    QPoint startPos;
    QList<QGraphicsItem*> items;
    Y_Image_Drawer_GUI::YGraphicsItem* focusItem = nullptr;
    Y_Image_Drawer_GUI::YGraphicsMosaicItem* mosaic;

protected:
    bool eventFilter(QObject*, QEvent*) override;
};

#endif // YIMAGEDRAWER_H
