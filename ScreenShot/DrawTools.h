﻿#ifndef DRAWTOOLS_H
#define DRAWTOOLS_H

#include "YToolWidget.h"

namespace Ui {
class DrawTools;
}

class DrawTools : public YToolWidget
{
    Q_OBJECT
public:
    explicit DrawTools(QWidget *parent = nullptr);
    ~DrawTools();
    int getThickness();
    QFont getFont();
    QColor getColor();
    int getVague();

    void setThickness(int thickness);
    void setColor(int index);
    void setVague(int value);

    void setStackIndex(int widget = -1);

signals:
    void thicknessChanged(int);
    void colorChanged(QColor);
    void fontChanged(const QFont&);
    void vagueChanged(int);

public slots:
    void setFont();

private:
    Ui::DrawTools *ui;
    int thickness = 1;
    QColor color = QColor(251,56,56);
    QFont font;
    int vague;
};

#endif // DRAWTOOLS_H
