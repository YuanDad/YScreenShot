﻿#ifndef SCREENSHOTWIDGET_H
#define SCREENSHOTWIDGET_H

#include <QWidget>
#include <QVector>

class YImageDrawer;
class YMagnifier;
class Tools;
class DrawTools;
class ScreenShotWidget : public QWidget
{
    Q_OBJECT

    enum CursorState {None=0, Left, TopLeft, Top, TopRight, Right, BottomRight, Bottom, BottomLeft, Center};
public:
    explicit ScreenShotWidget(QWidget *parent = nullptr);
    ~ScreenShotWidget() override;

signals:
    void deleteLated();
public slots:
private:
    YImageDrawer* drawer = nullptr;
    YMagnifier* magn;//弹出的放大镜
    Tools* tools;//工具栏
    DrawTools* drawTools;//绘画工具栏
    QImage image;//全屏截图
    QRect screenShotArea;//所选范围
    CursorState currentState = None;//当前鼠标所处区域
    bool pressed = false;//鼠标是否按下
    QPoint pressPoint;//鼠标按下坐标
    QPoint moveTopLeftPoint;//点击移动,记录左上角坐标用来对比
    int moveEdgeState = 0;//0:无状态,1:只改变宽度,2:只改变高度,3:只改变位置

    QVector<QRect> outside;//外部
    QVector<QRect> edge;//四条边
    QVector<QRect> corner;//四个角

    QRect expectArea;//预估范围

private:
    void refreshScreenShotArea();//
    QImage getScreenShotImage();
    void initToolConnect();

protected:
    void paintEvent(QPaintEvent*) override;
    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void focusOutEvent(QFocusEvent *) override;
    void keyReleaseEvent(QKeyEvent*) override;
};

#endif // SCREENSHOTWIDGET_H
