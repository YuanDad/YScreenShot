#ifndef YMAGNIFIER_H
#define YMAGNIFIER_H

#include <QWidget>

class YMagnifier : public QWidget
{
    Q_OBJECT
public:
    explicit YMagnifier(QImage* image = nullptr);

    void setImage(const QImage& image);
    void set(const QPoint& pos);
    void unset();
    void rePos();
    void create(const QImage& part);
    void setRatio(int ratio = 3);

signals:
    void keyClicked(const Qt::Key&);

public slots:

public:
    int ratio = 3;
    QImage image;
    QImage* pImage;
    QImage map;
    QSize threshold;//右下角阈值，超过阈值，界面应该置于鼠标左上方
    QPoint point;
    QPoint centerPos;//中心点
    QColor currentColor;
private:
    void paintEvent(QPaintEvent*);
    void keyReleaseEvent(QKeyEvent*);
};

#endif // YMAGNIFIER_H
