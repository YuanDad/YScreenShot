#include "YCenterScrollArea.h"
#include <QLayout>

YCenterScrollArea::YCenterScrollArea(QWidget *parent) : QScrollArea(parent)
{
    setObjectName("centerScrollArea");
    setStyleSheet("#centerScrollArea, #scrollAreaWidgetContents{ background-color: rgba(0,0,0,0); border: none}");
}

void YCenterScrollArea::defaultLayout()
{
    QHBoxLayout* mainLayout = new QHBoxLayout;
    parentWidget()->setLayout(mainLayout);
    mainLayout->addWidget(this);
}

YCenterScrollArea* YCenterScrollArea::setCenterWidget(QWidget *centerWidget)
{
    setWidgetResizable(true);
    QWidget* scrollAreaWidgetContents = new QWidget(this);
    scrollAreaWidgetContents->setObjectName("scrollAreaWidgetContents");
    QGridLayout* gridLayout = new QGridLayout(scrollAreaWidgetContents);
    gridLayout->setSpacing(0);
    gridLayout->setContentsMargins(0, 0, 0, 0);

    gridLayout->addItem(new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding), 0, 1, 1, 1);

    gridLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Minimum), 1, 0, 1, 1);

    gridLayout->addWidget(centerWidget, 1, 1, 1, 1);

    gridLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Minimum), 1, 2, 1, 1);

    gridLayout->addItem(new QSpacerItem(10, 10, QSizePolicy::Minimum, QSizePolicy::Expanding), 2, 1, 1, 1);

    setWidget(scrollAreaWidgetContents);

    return this;
}
