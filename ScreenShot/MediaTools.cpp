﻿#include "MediaTools.h"
#include "ui_MediaTools.h"
#include <QFrame>

#include "QyMultiMedia/YAvi.h"
#include "QyMultiMedia/YGif.h"
#include "QyMultiMedia/FpsTimer.h"
#include "QyMultiMedia/MediaCommon.h"

MediaTools::MediaTools(QWidget *parent) :
    YToolWidget(parent),
    ui(new Ui::MediaTools)
{
    ui->setupUi(this);
    connect(ui->btnRecord, &QRadioButton::clicked, [this]{
        if(isStarted){
            save();
        }else{
            ui->btnAvi->setEnabled(false);
            ui->btnGif->setEnabled(false);
            ui->intFps->setEnabled(false);
            isStarted = true;
            ui->btnRecord->setText("结束录制");
            start();
        }
    });
    ui->btnCancel->setText(QChar(0xf00d));
}

MediaTools::~MediaTools()
{
    stop();
    for(QWidget* line : lines){
        line->deleteLater();
    }
    delete ui;
}

QFrame* getOneFrame()
{
    static QString styleSheet = "background-color: red;";
    QFrame* line = new QFrame;
    line->setWindowFlags(Qt::ToolTip | Qt::WindowStaysOnTopHint | Qt::FramelessWindowHint);
    line->setStyleSheet(styleSheet);
    return line;
}

void MediaTools::tack(const QRect &rect)
{
    recordArea = rect;
    set(rect);

    if(rect.x() > 0){
        QFrame* line = getOneFrame();
        line->setGeometry(rect.x() - 1, rect.y() - 1, 1, rect.height() + 2);
        line->show();
        lines.append(line);
    }
    if(rect.y() > 0){
        QFrame* line = getOneFrame();
        line->setGeometry(rect.x() - 1, rect.y() - 1, rect.width() + 2, 1);
        line->show();
        lines.append(line);
    }
    if(rect.right() < desktopSize.width()){
        QFrame* line = getOneFrame();
        line->setGeometry(rect.right() + 1, rect.y() - 1, 1, rect.height() + 2);
        line->show();
        lines.append(line);
    }
    if(rect.bottom() < desktopSize.height()){
        QFrame* line = getOneFrame();
        line->setGeometry(rect.x() + 1, rect.bottom() + 1, rect.width() + 2, 1);
        line->show();
        lines.append(line);
    }
}

void MediaTools::timeout()
{
    if(selected) return;
    YToolWidget::timeout();
    for(QWidget* line : lines){
        line->raise();
    }
}

void MediaTools::start()
{
    if(ui->btnAvi->isChecked()){
        type = AVI;
        media = new YAvi(this);
    }else{
        type = GIF;
        media = new YGif(this);
    }
    media->open();
    media->setVideo(recordArea.width(), recordArea.height(), ui->intFps->value());

    timer = new FpsTimer(ui->intFps->value(), this);
    connect(timer, &FpsTimer::timeout, [this]{
        media->addFrame(MediaCommon::grabWindow(0, recordArea.x(), recordArea.y(), recordArea.width(), recordArea.height()));
    });
    timer->start();
}

void MediaTools::save()
{
    printf(__func__);
    timer->stop();
    selected = true;
    QString fileName = media->defaultFileDialog();
    selected = false;
    if(fileName.isEmpty()) return;
    media->save(fileName);
    deleteLater();
}

void MediaTools::stop()
{
//    if(type == AVI){
//    }else /*if(type == GIF)*/{
//    }
    if(media)
        media->close();
}
