#ifndef YUAN_H
#define YUAN_H

#include <QCursor>
#include <functional>

class QWidget;
class Yuan
{
public:
    Yuan();
    static QCursor CustomCursor();
    static QRect validRect();
    static QWidget* triggerWidget(std::function<void()> func, QWidget* parent);
};

#endif // YUAN_H
