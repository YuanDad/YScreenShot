#ifndef MEDIATOOLS_H
#define MEDIATOOLS_H

#include "YToolWidget.h"

namespace Ui {
class MediaTools;
}

enum MediaType{
    AVI, GIF
};
class YMedia;
class FpsTimer;
class MediaTools : public YToolWidget
{
    Q_OBJECT

public:
    explicit MediaTools(QWidget *parent = nullptr);
    ~MediaTools() override;

    void tack(const QRect& rect);

protected:
    void timeout() override;
    void start();
    void save();
    void stop();

private:
    Ui::MediaTools *ui;
    QRect recordArea;
    QWidgetList lines;
    bool isStarted = false;
    bool selected = false;
    MediaType type;

    FpsTimer* timer = nullptr;
    YMedia* media = nullptr;
};

#endif // MEDIATOOLS_H
