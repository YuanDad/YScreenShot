#ifndef YMEDIA_H
#define YMEDIA_H

#include <QObject>

class YMedia: public QObject
{
public:
    YMedia(QObject* parent = nullptr);

    virtual void setTempFileName();
    virtual bool open() = 0;
    inline bool isOpen() { return isOpened; }
    virtual void setVideo(int width, int height, double fps, const QString& extra = "MJPG") = 0;
    virtual void addFrame(const QImage& frame) = 0;
    virtual int close() = 0;
    virtual bool save(const QString& outputFileName) = 0;
    virtual QString saveFileType() = 0;
    virtual QString defaultFileDialog() = 0;

protected:
    bool isOpened = false;
    QString tempFileName;
};

#endif // YMEDIA_H
