#include "YMedia.h"
#include <QDir>
#include <QUuid>
#include <QFileInfo>

YMedia::YMedia(QObject *parent) : QObject(parent)
{}

void YMedia::setTempFileName()
{
    forever{
        tempFileName = QDir::tempPath() + '/' + QUuid::createUuid().toString() + ".media";
        if(!QFileInfo(tempFileName).exists()) break;
    }
}
