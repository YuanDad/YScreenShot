#ifndef YGIF_H
#define YGIF_H

#include "YMedia.h"

class Gif;
class YGif : public YMedia
{
public:
    YGif(QObject* parent = nullptr);
    ~YGif() override;

    bool open() override;
    inline bool isOpen() { return isOpened; }
    void setVideo(int width, int height, double fps, const QString& = QString()) override;
    void addFrame(const QImage& frame) override;
    int close() override;
    bool save(const QString& outputFileName) override;
    QString saveFileType() override;
    QString defaultFileDialog() override;

private:
    Gif* gif;
    quintptr gifWriter = 0;
    uint32_t frameWidth, frameHeight, frameFps;
};

#endif // YGIF_H
