#ifndef MEDIACOMMON_H
#define MEDIACOMMON_H

#include <QString>
#include <QTime>
#include <QImage>

class MediaCommon
{
public:
    static char* QStringToChars(const QString& qstring);
    static inline int currentMillSecond() { return QTime::currentTime().msec(); }
    static QImage grabWindow(WId window = 0, int x = 0, int y = 0, int w = -1, int h = -1);
};

#endif // MEDIACOMMON_H
