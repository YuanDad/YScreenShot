#include <QApplication>
#include <QDebug>
#include <QTime>
#include <QTimer>
#include "FpsTimer.h"

int main(int argc, char* argv[])
{
    QApplication application(argc, argv);

    FpsTimer fps(15);
    fps.start();

    int count = 0;
    fps.connect(&fps, &FpsTimer::timeout, [&fps, &count]{
        if(fps.getCurrentIndex() == 0)
            qDebug() << fps.startMillSecond << QTime::currentTime().msec();
        ++count;
        if(count >= 240) fps.stop();
    });

    return application.exec();
}
