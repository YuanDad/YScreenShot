#ifndef FPSTIMER_H
#define FPSTIMER_H

#include <QObject>

class QTimer;
class FpsTimer : public QObject
{
    Q_OBJECT
public:
    explicit FpsTimer(int fps, QObject *parent = nullptr);

public:
    void start();
    void stop();
    inline int getCurrentIndex() { return currentIndex; }

signals:
    void timeout();

private slots:
    void timerTimeOut();

public:
    QTimer* timer;
    int fps, unit, remainder, error, estimate;
    int startMillSecond, lastMillSecond, currentIndex;
};

#endif // FPSTIMER_H
