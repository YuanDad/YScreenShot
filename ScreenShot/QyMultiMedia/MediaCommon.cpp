#include "MediaCommon.h"
#include <QApplication>
#include <QScreen>
#include <QPixmap>

char* MediaCommon::QStringToChars(const QString& qstring)
{
    std::string stdString = qstring.toLocal8Bit().toStdString();
    char* cString = new char[stdString.size() + 1];
    strcpy(cString, stdString.c_str());
    cString[stdString.size()] = 0;
    return cString;
}

QImage MediaCommon::grabWindow(WId window, int x, int y, int w, int h)
{
    QScreen* desktop = qApp->primaryScreen();
    return desktop->grabWindow(window, x, y, w, h).toImage();
}

