#ifndef YAVI_H
#define YAVI_H

#include <QString>
#include <QImage>
#include "YMedia.h"

class YAvi : public YMedia
{
public:
    YAvi(QObject* parent = nullptr);
    ~YAvi() override;

    bool open() override;
    inline bool isOpen() { return isOpened; }
    void setVideo(int width, int height, double fps, const QString& extra = "MJPG") override;
    void addFrame(const QImage& frame) override;
    void setRadio(int channels, long rate, int bits, int format, long mp3rate);
    int close() override;
    bool save(const QString& outputFileName) override;
    QString saveFileType() override;
    QString defaultFileDialog() override;

private:
    quintptr outFd = 0;
    bool notFirstFrame = false;
};

#endif // YAVI_H
