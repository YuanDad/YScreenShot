﻿#include "Tools.h"
#include "ui_Tools.h"

Tools::Tools(QWidget *parent) :
    YToolWidget(parent),
    ui(new Ui::Tools)
{
    ui->setupUi(this);

    ui->radioButton_1->setText(QChar(0xf24d));
    ui->radioButton_2->setText(QChar(0xf1db));
    ui->radioButton_3->setText(QChar(0xf178));
    ui->radioButton_4->setText(QChar(0xf040));
    ui->radioButton_5->setText(QChar(0xf1a5));
    ui->radioButton_6->setText("A");
    ui->radioButton_7->setText(QChar(0xf25d));

    ui->pushButton_1->setText(QChar(0xf0e2));//redo
    ui->pushButton_2->setText(QChar(0xf0c4));
    ui->pushButton_3->setText(QChar(0xf1ab));
    ui->pushButton_4->setText(QChar(0xf2b1));
    ui->pushButton_5->setText(QChar(0xf1fb));//钉在桌面
    ui->pushButton_6->setText(QChar(0xf192));//录制
    ui->pushButton_7->setText(QChar(0xf019));//下载
    ui->pushButton_8->setText(QChar(0xf10b));
    ui->pushButton_9->setText(QChar(0xf02b));
    ui->pushButton_10->setText(QChar(0xf00d));//取消
    ui->pushButton_11->setText(QString(QChar(0xf00c)) + "完成");//完成

    connect(ui->radioButton_1, &QRadioButton::toggled, [this]{emit draw(1);});
    connect(ui->radioButton_2, &QRadioButton::toggled, [this]{emit draw(2);});
    connect(ui->radioButton_3, &QRadioButton::toggled, [this]{emit draw(3);});
    connect(ui->radioButton_4, &QRadioButton::toggled, [this]{emit draw(4);});
    connect(ui->radioButton_5, &QRadioButton::toggled, [this]{emit draw(5);});
    connect(ui->radioButton_6, &QRadioButton::toggled, [this]{emit draw(6);});
    connect(ui->radioButton_7, &QRadioButton::toggled, [this]{emit draw(7);});

    connect(ui->pushButton_1, &QPushButton::clicked, this, &Tools::redo);
    connect(ui->pushButton_5, &QPushButton::clicked, [this]{
        emit tack();
        emit finished();
    });
    connect(ui->pushButton_6, &QPushButton::clicked, [this]{
        emit record();
        emit finished();
    });
    connect(ui->pushButton_7, &QPushButton::clicked, [this]{
        emit download();
        emit finished();
    });
    connect(ui->pushButton_10, &QPushButton::clicked, [this]{
        emit finished();
    });
    connect(ui->pushButton_11, &QPushButton::clicked, [this]{
        emit clipBoard();
        emit finished();
    });

    ui->pushButton_2->hide();
    ui->pushButton_3->hide();
    ui->pushButton_4->hide();
    ui->pushButton_8->hide();
    ui->pushButton_9->hide();
    resize(width() - (ui->pushButton_2->width() + ui->horizontalLayout_2->margin()) * 5, height());
}

Tools::~Tools()
{
    delete ui;
}

void Tools::setTack()
{
    ui->radioBox->hide();
    ui->split1->hide();

    ui->pushButton_1->hide();
    ui->pushButton_5->hide();
    ui->pushButton_6->hide();
    resize(144, height());
}
