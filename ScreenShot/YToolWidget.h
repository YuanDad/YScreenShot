﻿#ifndef YTOOLWIDGET_H
#define YTOOLWIDGET_H

#include <QWidget>

class YToolWidget : public QWidget
{
    Q_OBJECT
public:
    explicit YToolWidget(QWidget *parent = nullptr);

    void set(const QRect& rect);
    void set(const QPoint& pos, const QSize& size = QSize());
    void unset();
    inline QSize getDesktopSize() { return desktopSize; }

public:
    bool toped = false;

protected:
    virtual void timeout();
    QSize desktopSize;
};

#endif // YTOOLWIDGET_H
