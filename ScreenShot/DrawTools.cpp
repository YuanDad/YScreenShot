﻿#include "DrawTools.h"
#include "ui_DrawTools.h"

static QColor colors[16] = {
    QColor(0,0,0), QColor(255,255,255), QColor(128,128,128), QColor(170,0,0),
    QColor(255,170,127), QColor(0,170,0), QColor(0,85,255), QColor(170,0,127),
    QColor(85,170,127),QColor(192,192,192),QColor(251,56,56),QColor(255,255,0),
    QColor(170,255,0),QColor(85,170,255),QColor(255,0,255),QColor(85,255,255)
};

DrawTools::DrawTools(QWidget *parent) :
    YToolWidget(parent),
    ui(new Ui::DrawTools)
{
    ui->setupUi(this);

    connect(ui->radioButton_1, &QRadioButton::toggled, [this](bool checked){if(checked)setThickness(1);});
    connect(ui->radioButton_2, &QRadioButton::toggled, [this](bool checked){if(checked)setThickness(2);});
    connect(ui->radioButton_3, &QRadioButton::toggled, [this](bool checked){if(checked)setThickness(3);});

    connect(ui->pushButton_2, &QPushButton::clicked, [this]{setColor(0);});
    connect(ui->pushButton_3, &QPushButton::clicked, [this]{setColor(1);});
    connect(ui->pushButton_4, &QPushButton::clicked, [this]{setColor(2);});
    connect(ui->pushButton_5, &QPushButton::clicked, [this]{setColor(3);});
    connect(ui->pushButton_6, &QPushButton::clicked, [this]{setColor(4);});
    connect(ui->pushButton_7, &QPushButton::clicked, [this]{setColor(5);});
    connect(ui->pushButton_8, &QPushButton::clicked, [this]{setColor(6);});
    connect(ui->pushButton_9, &QPushButton::clicked, [this]{setColor(7);});
    connect(ui->pushButton_10, &QPushButton::clicked, [this]{setColor(8);});
    connect(ui->pushButton_11, &QPushButton::clicked, [this]{setColor(9);});
    connect(ui->pushButton_12, &QPushButton::clicked, [this]{setColor(10);});
    connect(ui->pushButton_13, &QPushButton::clicked, [this]{setColor(11);});
    connect(ui->pushButton_14, &QPushButton::clicked, [this]{setColor(12);});
    connect(ui->pushButton_15, &QPushButton::clicked, [this]{setColor(13);});
    connect(ui->pushButton_16, &QPushButton::clicked, [this]{setColor(14);});
    connect(ui->pushButton_17, &QPushButton::clicked, [this]{setColor(15);});

    connect(ui->comboBox, &QComboBox::currentTextChanged, this, &DrawTools::setFont);
    setFont();
    connect(ui->horizontalSlider, &QSlider::valueChanged, this, &DrawTools::setVague);
    setVague(ui->horizontalSlider->value());
}

DrawTools::~DrawTools()
{
    delete ui;
}

int DrawTools::getThickness()
{
    return thickness;
}

QFont DrawTools::getFont()
{
    return font;
}

QColor DrawTools::getColor()
{
    return color;
}

int DrawTools::getVague()
{
    return vague;
}

void DrawTools::setThickness(int thickness)
{
    this->thickness = thickness;
    emit thicknessChanged(thickness);
}

void DrawTools::setColor(int index)
{
    if(index < 0 || index > 15) return;
    color = colors[index];
    ui->pushButton->setStyleSheet(QString("background-color: rgb(%1, %2, %3);")
                                  .arg(color.red()).arg(color.green()).arg(color.blue()));
    emit colorChanged(color);
}

void DrawTools::setFont()
{
    font = QFont("宋体", ui->comboBox->currentText().toInt());
    emit fontChanged(font);
}

void DrawTools::setVague(int value)
{
    vague = value;
    emit vagueChanged(vague);
}

void DrawTools::setStackIndex(int widget)
{
    if(widget == -1){
        ui->stackedWidget->setCurrentIndex(0);
        ui->stackedWidget_2->setCurrentIndex(0);
    }else{
        (widget == 0 ? ui->stackedWidget : ui->stackedWidget_2)->setCurrentIndex(1);
    }
}
